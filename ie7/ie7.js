/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'tjetak-icons\'">' + entity + '</span>' + html;
	}
	var icons = {
		'ti-cart-outline-4': '&#xe905;',
		'ti-icon_account': '&#xe900;',
		'ti-icon_quotation': '&#xe901;',
		'ti-icon_home': '&#xe902;',
		'ti-icon_transaction': '&#xe903;',
		'ti-icon_contact': '&#xe904;',
		'ti-add_location': '&#xe567;',
		'ti-cloud_upload': '&#xe2c3;',
		'ti-phone': '&#xe0cd;',
		'ti-navigate_next': '&#xe409;',
		'ti-keyboard_arrow_down': '&#xe313;',
		'ti-mail_outline': '&#xe0e1;',
		'ti-mug': '&#xe9a2;',
		'ti-whatsapp': '&#xea93;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/ti-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
