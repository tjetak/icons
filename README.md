# Tjetak Icons

Tjetak design icons consists of icons used throughout Tjetak applications.

## Installation

```bash
npm install @tjetak/icons
```

## Usage

### Via CSS or SCSS

You may import the icons inside CSS file:

```css
@import "~@tjetak/icons/style.css"
```

Or SCSS file:

```scss
import "~@tjetak/icons/style"
```

Then, you can use the icon in an HTML file:

```html
<i class="ti-phone"></i>
```

### Via font file

You may directly use the fonts located in [fonts]('https://gitlab.com/tjetak/icons/tree/master/fonts') folder.