# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.14](https://gitlab.com/tjetak/icons/compare/v1.0.13...v1.0.14) (2020-10-09)

### [1.0.13](https://gitlab.com/tjetak/icons/compare/v1.0.12...v1.0.13) (2020-10-09)

### [1.0.12](https://gitlab.com/tjetak/icons/compare/v1.0.11...v1.0.12) (2020-10-09)

### [1.0.11](https://gitlab.com/tjetak/icons/compare/v1.0.10...v1.0.11) (2019-12-20)


### Features

* Added icons ([c4e625d](https://gitlab.com/tjetak/icons/commit/c4e625d7a43f95fa7fde6124e19a5f4313c14679))

### [1.0.10](https://gitlab.com/tjetak/icons/compare/v1.0.9...v1.0.10) (2019-11-05)


### Features

* Add more icon for Tjetak Ops ([af0e494](https://gitlab.com/tjetak/icons/commit/af0e494))

### [1.0.9](https://gitlab.com/tjetak/icons/compare/v1.0.8...v1.0.9) (2019-10-16)

### [1.0.8](https://gitlab.com/tjetak/icons/compare/v1.0.7...v1.0.8) (2019-10-07)

### [1.0.7](https://gitlab.com/tjetak/icons/compare/v1.0.4...v1.0.7) (2019-08-02)


### Features

* Add more icon for Landing Page Pro ([df5f5c8](https://gitlab.com/tjetak/icons/commit/df5f5c8))
* Add more icon for Tjetak Plain Packaging, Plus, Ops, and Support ([a07d91e](https://gitlab.com/tjetak/icons/commit/a07d91e))
* Add new Icon for Tjetak support ([d552c4c](https://gitlab.com/tjetak/icons/commit/d552c4c))
* Add new Icon for Tjetak Support, Ops, Pro, & Plain Packaging ([f8efc3e](https://gitlab.com/tjetak/icons/commit/f8efc3e))

### [1.0.5](https://gitlab.com/tjetak/icons/compare/v1.0.4...v1.0.5) (2019-08-02)


### Features

* Add more icon for Landing Page Pro ([df5f5c8](https://gitlab.com/tjetak/icons/commit/df5f5c8))
* Add more icon for Tjetak Plain Packaging, Plus, Ops, and Support ([a07d91e](https://gitlab.com/tjetak/icons/commit/a07d91e))
* Add new Icon for Tjetak support ([d552c4c](https://gitlab.com/tjetak/icons/commit/d552c4c))
* Add new Icon for Tjetak Support, Ops, Pro, & Plain Packaging ([f8efc3e](https://gitlab.com/tjetak/icons/commit/f8efc3e))

### [1.0.4](https://gitlab.com/tjetak/icons/compare/v1.0.0...v1.0.4) (2019-07-01)


### Features

* Add new Icon for Tjetak Support & Tjetak Pro ([7c9242e](https://gitlab.com/tjetak/icons/commit/7c9242e))



## [1.0.3](https://gitlab.com/tjetak/icons/compare/v1.0.0...v1.0.3) (2019-06-12)



## [1.0.2](https://gitlab.com/tjetak/icons/compare/v1.0.0...v1.0.2) (2019-05-20)



## [1.0.1](https://gitlab.com/tjetak/icons/compare/v1.0.0...v1.0.1) (2019-04-23)



# [1.0.0](https://gitlab.com/tjetak/icons/compare/v1.0.0-beta.3...v1.0.0) (2019-04-23)


### Bug Fixes

* Fix typo ([a5182c6](https://gitlab.com/tjetak/icons/commit/a5182c6))



# [1.0.0-beta.4](https://gitlab.com/tjetak/icons/compare/v1.0.0-beta.3...v1.0.0-beta.4) (2019-03-22)


### Bug Fixes

* Fix typo ([a5182c6](https://gitlab.com/tjetak/icons/commit/a5182c6))



# [1.0.0-beta.3](https://gitlab.com/tjetak/icons/compare/v1.0.0-beta.2...v1.0.0-beta.3) (2019-03-22)


### Features

* Added sizes ([f1e6e99](https://gitlab.com/tjetak/icons/commit/f1e6e99))



# [1.0.0-beta.2](https://gitlab.com/tjetak/icons/compare/v1.0.0-beta.1...v1.0.0-beta.2) (2019-03-22)



# [1.0.0-beta.1](https://gitlab.com/tjetak/icons/compare/v1.0.0-beta.0...v1.0.0-beta.1) (2019-03-14)
